;
; SP_SEK: Autostart f�r ein Programm auf den letzten 4 Sektoren
;
; Letzte Bearbeitung:  4.5.1994
;
; PC-relativ kompilieren : 404 Bytes.
;
; (c) Markus Hoffmann 1992

SP_SEK_LOADER:                  ; a0= Adresse Bootsektor
      move.b $14(a0),d0         ; Highbyte anzahl sektoren auf disk
      lsl    #8,d0
      move.b $13(a0),d0         ; Lowbyte
      subq   #4,d0              ; -4
      lea    main(PC),a1
      move   d0,(sp_sek-main)(a1)

      move.l $436,a2            ; Kopiert die Routine aus
      lea    -(ende-main)(a2),a2      ; dem Diskbuffer raus
      move.l a2,a3
      move   #(ende-main)/2-1,d5
      BSR    COPY
      jmp    weiter-main(a3)

MAIN: dc.b 'SP_SEK Bootsektorlader (c) Markus Hoffmann 1992 V.2.01',0
          align
sp_sek:   dc.w 1638
sp_puf:   dc.l $DEADFACE
BPB:      dc.l 'M.H.'

weiter:  move.l #2048,-(sp)      ; MALLOC  f�r 4 Sektoren
         move #72,-(sp)
         trap #1
         addq.l #6,sp
         lea    sp_puf(pc),a0
         MOVE.L D0,(a0)
         tst.l  d0
         bmi.s  error

         bsr.s  bios7
         lea    BPB(PC),a0
         move.l d0,(a0)

         clr               -(sp)  ; Laufwerk A:
         move   sp_sek(pc),-(sp)  ; Ab Sektor 1636
         move           #4,-(sp)  ; 4 Sektoren
         move.l sp_puf(pc),-(sp)  ; in Buffer
         clr               -(sp)  ; Laden
         move           #4,-(sp)  ; BIOS 4
         trap          #13
         lea        14(sp),sp
         tst.l          d0
         bmi.s       error

         move.l  sp_puf(pc),a0
         move.l  a0,a1
         moveq.l #0,d0
         move    #1024-1,d5       ; 1024 Worte
\hook    add     (a1)+,d0         ; Checksumme
         DBRA    D5,\hook
         cmp     #'PR',d0         ; 'Text'
         bne.s   s1
         bsr.s   print
         bsr.s   free
ret:     rts
s1:      cmp    #'RN',d0          ; 'Programm'
         bne.s  s2
     ; Die Routine erh�lt in den Registern:
     ;    a0= Adresse von sich selbst (= Adresse des MALLOC-Speichers)
     ;    a1= Adresse des Bios-Parameterblocks der Disk
     ;    a2= Adresse zum Beenden des Programms (nicht resistent)
     ;    a3= Routine Bildschirmausgabe (nicht resistent)
     ;    a4= Routine d0 to dez         (      "     )
     ;    a5= Routine Gemdos7           (       ")
     ;    d0='RN'
     ;    d1=Startsektor auf Disk (1638)
         move.l       BPB(pc),a1
         lea         free(pc),a2
         lea        print(pc),a3
         lea    d0_to_dez(pc),a4
         lea         inp2(pc),a5
         move      sp_sek(pc),d1
         jmp    (a0)

S2:      lea    string(pc),a0
         bra.s  printfree
error:   lea    strin2(pc),a0
printfree bsr.s  print
         bsr.s  free
         rts
bios7:   clr    -(sp)         ; Bios 7
         move   #7,-(sp)
         trap   #13
         addq.l #4,sp
         rts
free:    move.l sp_puf(pc),-(sp)
         move    #73,-(sp)
         trap    #1
         addq.l  #6,sp
         rts
print:   movem.l d0-a6,-(sp)
         move.l a0,-(sp)
         move   #9,-(sp)
         trap   #1
         addq.l #6,sp
         movem.l (sp)+,d0-a6
         rts
inp2:    movem.l d1-a6,-(sp)
         move    #7,-(sp)
         trap    #1
         addq.l  #2,sp
         movem.l (sp)+,d1-a6
         rts
; ####################################
; ## Routine wandelt wert in D0 in  ##
; ## Stringkette Dezimal, variabler ##
; ## L�nge. R�ckgabe: a0=Adresse    ##
; ## Zeichenkette                   ##
; ####################################
d0_to_dez:lea     dez_buf(pc),a0
          clr.b   (a0)
\nextdec  divu    #10,d0           ; in Dezimalstring wandeln
          swap    d0
          addi.b  #'0',d0
          move.b  d0,-(a0)
          clr.w   d0
          swap    d0
          tst     d0               ; a0= stringadresse
          bne.s   \nextdec
          rts
          dc.b '123456789'
dez_buf:  dc.b 0

string: dc.b 'Kein Virus im Bootsektor',13,10,0
strin2: dc.b 'BOOTSEKTOR-ERROR',13,10,0
        align
ende:   ; Ab hier wird nicht mehr mitkopiert
copy:    move  (a1)+,(a2)+
         dbra d5,copy
         rts


 end
 